package com.moravskiyandriy.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ApplicationTask2 {
    private static final Logger logger = LogManager.getLogger(ApplicationTask2.class);

    public static void main(String[] args) {
        Menu menu = new Menu();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            logger.info("\nMenu: ");
            menu.printMenu();
            logger.info("Your choise:");
            String choise = scanner.nextLine();
            if (choise.equals("quit")) {
                break;
            }
            menu.chose(Integer.valueOf(choise));
        }
    }
}
