package com.moravskiyandriy.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

class Menu {
    private static final Logger logger = LogManager.getLogger(Menu.class);
    private CommandSwitcher commandSwitcher;
    private List<String> menu = new ArrayList<>();

    Menu() {
        commandSwitcher = new CommandSwitcher();
        commandSwitcher.addCommand(new Command() {
            @Override
            public void execute() {
                logger.info("Anonimus class.");
            }
        });
        menu.add("0. Anonimus class.");
        commandSwitcher.addCommand(() -> logger.info("Lambda expression."));
        menu.add("1. Lambda expression.");
        commandSwitcher.addCommand(new Method()::execute);
        menu.add("2. Method reference.");
    }

    void chose(final Integer number) {
        commandSwitcher.execute(number);
    }

    void printMenu() {
        for (String s : menu) {
            System.out.println(s);
        }
    }
}

