package com.moravskiyandriy.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Method implements Command {
    private static final Logger logger = LogManager.getLogger(Method.class);

    @Override
    public void execute() {
        logger.info("Object of class and method reference.");
    }
}
