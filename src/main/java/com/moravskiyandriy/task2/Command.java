package com.moravskiyandriy.task2;

public interface Command {
    void execute();
}
