package com.moravskiyandriy.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ApplicationTask4 {
    private static final Logger logger = LogManager.getLogger(ApplicationTask4.class);

    public static void main(String[] args) {
        StringBuilder text = new StringBuilder();
        Scanner scanner = new Scanner(System.in);
        logger.info("Input text: ");
        while (!scanner.next().equals("")) {
            text.append(scanner.nextLine());
            if (scanner.nextLine().isEmpty()) {
                break;
            }
        }
        new TextSummarizer(text.toString()).summarize();
    }
}
