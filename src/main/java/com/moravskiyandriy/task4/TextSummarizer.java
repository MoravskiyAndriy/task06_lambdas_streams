package com.moravskiyandriy.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.counting;

class TextSummarizer {
    private static final Logger logger = LogManager.getLogger(TextSummarizer.class);
    private String[] words;
    private String str;

    TextSummarizer(String s) {
        str = s;
        words = s.split(" ");
    }

    private void uniqueWordsNumber() {
        Stream<String> stream1 = Arrays.stream(words);
        logger.info("Number of unique words: " + stream1
                .distinct()
                .count());
    }

    private void listOfUniqueWords() {
        Stream<String> stream1 = Arrays.stream(words);
        logger.info("Sorted list of unique words: " + Arrays.toString(stream1
                .distinct()
                .sorted()
                .toArray()));
    }

    private void wordsFrequency() {
        List<String> list = Arrays.asList(words);
        Map<String, Long> countWords = list.stream()
                .map(String::toLowerCase)
                .collect(Collectors.groupingBy(Function.identity(), counting()));
        logger.info("Word's frequency: " + countWords);
    }

    private void charactersFrequency() {
        Map<String, Long> frequentChars = Arrays.stream(
                str.toLowerCase().split(""))
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        logger.info("Characters frequency: " + frequentChars);
    }

    void summarize() {
        uniqueWordsNumber();
        listOfUniqueWords();
        wordsFrequency();
        charactersFrequency();
    }
}
