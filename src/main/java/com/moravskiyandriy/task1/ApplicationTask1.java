package com.moravskiyandriy.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ApplicationTask1 {
    private static final Logger logger = LogManager.getLogger(ApplicationTask1.class);

    @FunctionalInterface
    interface CustomFI {
        int getResult(int a, int b, int c);
    }

    public static void main(String[] args) {
        CustomFI lambda1 = (a, b, c) -> Math.max(Math.max(a, b), c);
        CustomFI lambda2 = (a, b, c) -> (a + b + c) / 3;
        logger.info(lambda1.getResult(1, 8, 4));
        logger.info(lambda2.getResult(2, 8, 4));
    }
}
