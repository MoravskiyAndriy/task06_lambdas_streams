package com.moravskiyandriy.task3;

class Constants {
    private Constants(){}

    static final int SIZE = 100;
    static final int MIN_NUMBER = 10;
    static final int MAX_NUMBER = 100;
}
