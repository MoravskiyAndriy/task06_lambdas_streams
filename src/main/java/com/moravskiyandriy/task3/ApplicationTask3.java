package com.moravskiyandriy.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;

public class ApplicationTask3 {
    private static final Logger logger = LogManager.getLogger(ApplicationTask3.class);

    public static void main(String[] args) {
        List<Integer> stream1 = new Randomizers().randomizer1();
        logger.info("MIN: "+stream1.stream()
                .min(Comparator.comparing(Integer::valueOf))
                .get());
        logger.info("MAX: "+stream1.stream()
                .max(Comparator.comparing(Integer::valueOf))
                .get());
        logger.info("Avarage: "+stream1.stream()
                .mapToInt(a -> a)
                .average());
        logger.info("Less then average: "+stream1.stream()
                .filter(a -> a < stream1.stream().mapToInt(b -> b).sum() / (long) stream1.size())
                .count());
        logger.info("Sum: "+stream1.stream().reduce(Integer::sum) + "\n");
    }
}
