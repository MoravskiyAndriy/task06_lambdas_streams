package com.moravskiyandriy.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Randomizers {
    private static final Logger logger = LogManager.getLogger(Randomizers.class);
    private int size;
    private int minNumber;
    private int maxNumber;

    Randomizers() {
        Properties prop = new Properties();
        try (InputStream input = Randomizers.class.getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (Exception ex) {
            logger.warn("Exception found.");
        }
        size = Optional.ofNullable(prop.getProperty("SIZE")).
                map(Integer::valueOf).orElse(Constants.SIZE);
        minNumber = Optional.ofNullable(prop.getProperty("MIN_NUMBER")).
                map(Integer::valueOf).orElse(Constants.MIN_NUMBER);
        maxNumber = Optional.ofNullable(prop.getProperty("MAX_NUMBER")).
                map(Integer::valueOf).orElse(Constants.MAX_NUMBER);
    }

    List<Integer> randomizer1() {
        IntStream stream = new Random().ints(size, minNumber, maxNumber);
        return stream.boxed().collect(Collectors.toList());
    }

    public List<Integer> randomizer2() {
        Random r = new Random();
        IntSupplier generator = new IntSupplier() {
            int current = 0;

            public int getAsInt() {
                return current + r.nextInt(maxNumber);
            }
        };
        IntStream stream = IntStream.generate(generator).limit(size);
        return stream.boxed().collect(Collectors.toList());
    }
}
